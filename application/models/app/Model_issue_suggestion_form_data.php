<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_issue_suggestion_form_data extends MY_Model
{
	protected $_table = 'issue_suggestion_form_data';

	protected $accessibleFields = ['user_id', 'office_id', 'reporting_week', 'subject', 'type', 'description'];

	function getAllFormData($showDeleted = false)
	{
		$this->db->group_start();
		$this->db->where('deleted', $showDeleted)->or_where('deleted', false);
		$this->db->group_end();
		$this->db->order_by('created_at', 'DESC');
		return $this->db->get($this->_table);
	}

	function getFormDataCount($showDeleted, $keyword, $where = [])
	{
		$this->db->join('users u', 'u.user_id = '.$this->_table.'.user_id');
		$this->db->join('offices o', 'o.office_id = '.$this->_table.'.office_id');

		$this->db->group_start();
		$this->db->where($where);
		$this->db->where($this->_table.'.deleted', $showDeleted);
		$this->db->group_end();

		if ($keyword)
		{
			$this->db->group_start();
			$this->db->like('u.title', $keyword);
			$this->db->or_like('u.first_name', $keyword);
			$this->db->or_like('u.last_name', $keyword);
			$this->db->or_like('o.office_name', $keyword);
			$this->db->or_like($this->_table.'.reporting_week::text', $keyword, 'both', false);
			$this->db->or_like($this->_table.'.subject::text', $keyword, 'both', false);
			$this->db->or_like($this->_table.'.type::text', $keyword, 'both', false);
			$this->db->or_like($this->_table.'.description::text', $keyword, 'both', false);
			$this->db->or_like($this->_table.'.created_at::text', $keyword, 'both', false);
			$this->db->group_end();
		}

		return $this->db->count_all_results($this->_table);
	}

	function getFormDataById($showDeleted = false, $issue_suggestion_form_data_id)
	{
		$this->db->select($this->_table.'.issue_suggestion_form_data_id,
						'.$this->_table.'.user_id,
						'.$this->_table.'.office_id,
						o.office_name,
						'.$this->_table.'.reporting_week,
						'.$this->_table.'.subject,
						'.$this->_table.'.type,
						'.$this->_table.'.description,
						'.$this->_table.'.created_at');

		$this->db->join('users u', 'u.user_id = '.$this->_table.'.user_id');
		$this->db->join('offices o', 'o.office_id = '.$this->_table.'.office_id');

		$this->db->where($this->_table.'.issue_suggestion_form_data_id', $issue_suggestion_form_data_id);

		$this->db->group_start();
		$this->db->where($this->_table.'.deleted', $showDeleted)->or_where($this->_table.'.deleted', false);
		$this->db->group_end();

		return $this->db->get($this->_table);
	}

	function getFormData($showDeleted = false, $start, $length, $order_by, $order, $keyword, $where = [])
	{
		$this->db->select($this->_table.'.issue_suggestion_form_data_id,
						   CONCAT_WS(\' \', u.title, u.first_name, u.last_name) AS submitted_by,
						   	o.office_name AS location,
						   	'.$this->_table.'.reporting_week,
						   	'.$this->_table.'.subject,
						   	'.$this->_table.'.type,
						   	'.$this->_table.'.description,
							'.$this->_table.'.created_at');
		$this->db->join('users u', 'u.user_id = '.$this->_table.'.user_id');
		$this->db->join('offices o', 'o.office_id = '.$this->_table.'.office_id');

		if ($keyword)
		{
			$this->db->group_start();
			$this->db->like('u.title', $keyword);
			$this->db->or_like('u.first_name', $keyword);
			$this->db->or_like('u.last_name', $keyword);
			$this->db->or_like('o.office_name', $keyword);
			$this->db->or_like($this->_table.'.subject::text', $keyword, 'both', false);
			$this->db->or_like($this->_table.'.type::text', $keyword, 'both', false);
			$this->db->or_like($this->_table.'.description::text', $keyword, 'both', false);
			$this->db->or_like($this->_table.'.created_at::text', $keyword, 'both', false);
			$this->db->group_end();
		}

		$this->db->where($where);

		$this->db->group_start();
		$this->db->where($this->_table.'.deleted', $showDeleted)->or_where($this->_table.'.deleted', false);
		$this->db->group_end();

		if ($this->input->post('start_date'))
			$this->db->where($this->_table.'.reporting_week>=', $this->input->post('start_date'));

		if ($this->input->post('end_date'))
			$this->db->where($this->_table.'.reporting_week<=', $this->input->post('end_date'));

		if ($length > 0)
     		$this->db->limit($length, $start);

	    $this->db->order_by($order_by, $order);

		return $this->db->get($this->_table);
	}

	function insertFormData($data)
	{
		$data = $this->permittedFields($data, $this->accessibleFields);
		$data = $this->updateTimestamps($data, 'insert');
		$this->writeDB->insert($this->_table, $data);
		return (($this->writeDB->affected_rows() == 1) ? $this->writeDB->insert_id() : false );
	}

	function getAllFormsData($fields, $where)
	{
		return $this->db->select($fields)
						->where($where)
						->get($this->_table);
	}

	function updateFormData($issue_suggestion_form_data_id, $data)
	{
		$data = $this->permittedFields($data, $this->accessibleFields, 'update');
		$data = $this->updateTimestamps($data, 'update');

		$this->writeDB->where('issue_suggestion_form_data_id', $issue_suggestion_form_data_id)
					  ->set($data)
					  ->update($this->_table);

		if ($this->writeDB->affected_rows() == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function deleteFormData($issue_suggestion_form_data_id)
	{
		$data = ['deleted' => true];
		$data = $this->updateTimestamps($data, 'delete');

		$this->writeDB->where('issue_suggestion_form_data_id', $issue_suggestion_form_data_id)
			->set($data)
			->update($this->_table);

		if ($this->writeDB->affected_rows() >= 1) {
			return true;
		} else {
			return false;
		}
	}

	public function issuesReport($showDeleted)
	{
		$this->db->select($this->_table.'.office_id,
						'.$this->_table.'.reporting_week, count(issue_suggestion_form_data_id) as issues_count');

		$this->db->where($this->_table.'.reporting_week>=', $this->input->post('start_date'));
		$this->db->where($this->_table.'.reporting_week<=', $this->input->post('end_date'));
		$this->db->where($this->_table.'.type', 'issue');

		if (sizeof($this->input->post('offices')))
			$this->db->where_in($this->_table.'.office_id', $this->input->post('offices'));

		$this->db->group_start();
		$this->db->where($this->_table.'.deleted', $showDeleted)->or_where($this->_table.'.deleted', false);
		$this->db->group_end();

		$this->db->order_by($this->_table.'.reporting_week', 'ASC');
		$this->db->group_by([$this->_table.'.office_id', $this->_table.'.reporting_week']);

		return $this->db->get($this->_table);
	}

	public function suggestionsReport($showDeleted)
	{
		$this->db->select($this->_table.'.office_id,
						'.$this->_table.'.reporting_week, count(issue_suggestion_form_data_id) as suggestions_count');

		$this->db->where($this->_table.'.reporting_week>=', $this->input->post('start_date'));
		$this->db->where($this->_table.'.reporting_week<=', $this->input->post('end_date'));
		$this->db->where($this->_table.'.type', 'suggestion');

		if (sizeof($this->input->post('offices')))
			$this->db->where_in($this->_table.'.office_id', $this->input->post('offices'));

		$this->db->group_start();
		$this->db->where($this->_table.'.deleted', $showDeleted)->or_where($this->_table.'.deleted', false);
		$this->db->group_end();

		$this->db->order_by($this->_table.'.reporting_week', 'ASC');
		$this->db->group_by([$this->_table.'.office_id', $this->_table.'.reporting_week']);

		return $this->db->get($this->_table);
	}
}

/* End of file Model_location_form_data.php */
