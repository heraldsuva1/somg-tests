<?php


use Phinx\Seed\AbstractSeed;

class IssueSuggestionSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
		$faker = Faker\Factory::create();
		$data = [];

		for ($i = 0; $i < 100; $i++) {
			$data[] = [
				'office_id' => $faker->randomElement(range(1,5)),
				'user_id' => 1,
				'reporting_week' => $faker->date(),
				'subject' => $faker->text(),
				'description' => $faker->paragraph(),
				'type' => $faker->randomElement(['issue', 'suggestion'])
			];
		}

		$m_issue_suggestion_form_data = $this->table('issue_suggestion_form_data');
		$m_issue_suggestion_form_data->insert($data)->saveData();
    }
}
